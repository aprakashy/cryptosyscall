#include"xcipher.h"


int main(int argc, char *const* argv){

	extern char* optarg;
	extern int optind;

	unsigned char *pass = NULL; 
	unsigned char hash[MD5_DIGEST_LENGTH];
	int c,flag = -1,man_args = 0, rc;
	userinfo uInfo;

	opterr = 0;
	while ((c = getopt (argc, argv, "c:dehp:")) != -1)
		switch (c)
		{
			case 'c':
				if(optarg != NULL && strlen(optarg) < 3){
					fprintf(stderr,"incorrect Argument for option -%c\n", c);
					fprintf(stderr,"For usage, use option -h\n");
					//exit(EXIT_FAILURE);
					
					printUsagAndExit();
				}
				uInfo.ciphertype = optarg;
				break;
			case 'd':				
				flag = XCRYPT_DECRYPT;
				uInfo.flag = flag;
				man_args++;
				break;
			case 'e':
				flag = XCRYPT_ENCRYPT;
				uInfo.flag = flag;
				man_args++;
				break;
			case 'h':
				printUsagAndExit();
				break;
			case 'p':
				if(optarg != NULL && strlen(optarg) < 6){
					fprintf(stderr,"Passphrase must be at least 6 character long\n");
					fprintf(stderr,"For usage, use option -h\n");
					printUsagAndExit();
				}
				pass = (unsigned char*)optarg; // use the key buff to 
				man_args++;
				MD5(pass, sizeof(pass),hash); //generate the key from the pass phrase.
				uInfo.keybuff[16] = 0;
				strcpy((char*)(uInfo.keybuff),(char*)hash);				
				break;

			case '?':
				if(c == 'c'){
					fprintf(stderr, "Argument for option = -%c is missing\n", c);
				}
				if(c == 'p'){
					fprintf(stderr, "Argument for option = -%c is missing\n", c);
				}
				printUsagAndExit();
			default:
				rc = -EINVAL;
				printUsagAndExit();	

		}
	if( uInfo.flag > 1 || uInfo.flag < 0){
		printUsagAndExit();	
	}

	//uInfo->flag = flag;
	if(argv[optind] == NULL){
		printUsagAndExit();	
	}	
	man_args++;
	uInfo.infile = argv[optind];
	optind++;
	if(argv[optind] == NULL){
		printUsagAndExit();
	}
	uInfo.outfile = argv[optind];
	man_args++;

	if( man_args < 4){
		fprintf(stderr,"Mandatory options are needed\n");
		printUsagAndExit();
	}
	rc = syscall(__NR_xcrypt, (void *) &uInfo);

	if (rc == 0)
		printf("syscall returned %d\n", rc);
	else
		printf("syscall returned %d (errno=%d)\n", rc, errno);
	exit(rc);
}


void printUsagAndExit(){
	fprintf(stderr,"USAGE: xcipher [-h] <-e|-d> <-p passphrase> [-c ciphertype] <SOURCEFILE> <DESTFILE>\n");
	fprintf(stderr,"For usage, use option -h\n");
	exit(EXIT_FAILURE);
}
