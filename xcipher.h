/*
 * FILE: xcipher.h
 * Author: Aditya Praksh
 * Organization: Stony Brook university
 * 
 * Purpose: Header file to implement system call for the crypto operations on 
 * 	    on files.
 *
 */

#include<stdio.h>
#include<asm/unistd.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include<errno.h>
#include<unistd.h>
#include<openssl/md5.h>
#include<sys/syscall.h>

#ifndef __NR_xcrypt
#error xcrypt system call not defined
#endif


#define XCRYPT_ENCRYPT 0
#define XCRYPT_DECRYPT 1


typedef struct packet{
        char *infile;
        char *outfile;
        int flag;
        unsigned int keylen;
	unsigned char keybuff[MD5_DIGEST_LENGTH];
        char *ciphertype;
}userinfo;


void printUsagAndExit();


