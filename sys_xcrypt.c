#include <linux/kernel.h>
#include <linux/unistd.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/fcntl.h>
#include <linux/sched.h>
#include <linux/linkage.h>
#include <linux/init.h>
#include <linux/crypto.h>
#include <linux/mm.h>
#include <linux/namei.h>
#include <linux/scatterlist.h>
#include <asm/scatterlist.h>
#include <asm/uaccess.h>
#include <linux/moduleloader.h>

#define READ_FLAG 1
#define WRITE_FLAG 2
#define ENCRYPT_FLAG 0
#define DECRYPT_FLAG 1
#define HASH_SIZE 16

typedef struct packet
{
	char *infile;
	char *outfile;
	int flag;
	unsigned int keylen;
	unsigned char keybuff[16];
	char *ciphertype;
} userinfo;



/**
 * file_op - reading from or writing to user level 
 * @filp: file pointer
 * @buf: buffer to read or write
 * @len: length
 * @flag: read or write flag( READ_FLAG or WRITE_FLAG)
 *
 * This function reads from or write to the user space file using VFS_read/write methods
 *
 * Returns zero on failure or number of bytes read.
 */
static int file_op(struct file* filp, char* buf, int len, int flag)
{
	mm_segment_t oldfs;
	int bytes=-EPERM;

	switch(flag)
	{
		case READ_FLAG: // read
			oldfs = get_fs();
			set_fs(KERNEL_DS);
			bytes = vfs_read(filp, buf, len, &filp->f_pos);
			set_fs(oldfs);
			break;

		case WRITE_FLAG: //write
			oldfs = get_fs();
			set_fs(KERNEL_DS);
			bytes = vfs_write(filp, buf, len, &filp->f_pos);
			set_fs(oldfs);
			break;

		default:
			printk(" File = %s, Line = %d , default mode for file given\n",__FILE__, __LINE__);
			break;

	}
	printk(" File = %s, Line = %d , bytes = %d\n",__FILE__, __LINE__, bytes);

return bytes;
}


/**
 * crypto_hash - this is used for generating the hash of the key while generation the preamble and 
 * verification of the preamble at the time of the decryption. 
 * @key: key buffer
 * @hashbuf: hash buffer
 * @len: length
 *
 * Returns zero on failure or number of bytes read.
 */
static int crypto_hash(char* key, char* hashbuf, int len)
{
	struct scatterlist hash_sg;
	struct hash_desc h_desc;
	int ret = 0;
	//hash operation
	h_desc.flags = 0;
	h_desc.tfm = crypto_alloc_hash("md5", 0, CRYPTO_ALG_ASYNC);
	if (!h_desc.tfm || IS_ERR(h_desc.tfm))
	{
		ret = -EFAULT;
		printk("Error attempting to allocate crypto context; ret = [%d]\n",ret);
		goto OUT_HASH;
	}

	sg_init_one(&hash_sg, key, len);
	printk(" File = %s, Line = %d , key = %s size[key] = %d\n",__FILE__, __LINE__,key, len );
	printk("calling hash init\n");
	ret = crypto_hash_init(&h_desc);
	if(ret)
	{
		printk("Error initializing the hash engine: [%d]\n",ret);
		goto OUT_HASH;
	}

	printk("calling hash update\n");
	ret = crypto_hash_update(&h_desc, &hash_sg, len);
	if(ret)
	{
		printk("Error updating the hash: [%d]\n",ret);
		goto OUT_HASH;
	}

	memset(hashbuf, 0x00, HASH_SIZE);
	printk("calling hash final\n");
	ret = crypto_hash_final(&h_desc, hashbuf);
	printk(" Final Hashbuf: [%s]\n",hashbuf);
	if(ret)
	{
		printk("Error final Hash: [%d]\n",ret);
		goto OUT_HASH;
	}

OUT_HASH:
	printk(" hash:= [%s]\n",hashbuf);
	if(h_desc.tfm){
		crypto_free_hash(h_desc.tfm);
		h_desc.tfm = NULL;
	}
	return ret;
}


/**
 * crypto_op - This funtion is used for the crypto operation like encryption or decryption. 
 * @keybuf: key buffer
 * @inbuf: input buffer which is eith encrypted or decrypted depending upon the Flag
 * @len: length
 * @flag: ENCRYPT_FLAG OR DECRYPT_FLAG
 * Returns zero on success or else faiure.
 */
static int crypto_op(char * inbuf, char* outbuf, char* keybuf, int len,int flag)
{
	struct crypto_blkcipher *blkcipher = NULL;
	char *blkcipher_alg = "ctr(aes)";
	char *iv = NULL;
	struct blkcipher_desc blk_desc;
	struct scatterlist sg1[1];
	struct scatterlist sg2[1];
	int ret = 0;

	printk("FILE = %s, LINE = %d, Len = %d len[inbuf] = %d len[keybuf]= %d\n", __FILE__, __LINE__, len, strlen(inbuf), strlen(keybuf));

	blkcipher = crypto_alloc_blkcipher(blkcipher_alg , 0, CRYPTO_ALG_ASYNC);
	if (!blkcipher || IS_ERR(blkcipher))
	{
		printk("could not allocate blkcipher handle\n");
		ret = -EFAULT;
		goto OUT_CRYPTO;
	}

	printk(" Setting Key for %s\n", keybuf);
	if (crypto_blkcipher_setkey(blkcipher, keybuf, strlen(keybuf)))
	{
		printk("key could not be set\n");
		ret = -EAGAIN;
		goto OUT_CRYPTO;
	}

	blk_desc.flags = 0;
	blk_desc.tfm = blkcipher;

	printk(" Encryption Key is set \n");
	printk("setting sg init for inbuf = %s\n", inbuf);
	sg_init_one(sg1, inbuf, len);
	sg_init_one(sg2, outbuf, len);

	/* Initialize IV */
	iv = kmalloc( crypto_blkcipher_ivsize( blk_desc.tfm ), GFP_KERNEL );
	if(iv == NULL ) {
		ret = -ENOMEM;
		goto OUT_CRYPTO;
	}
		
	memset( iv, 0, crypto_blkcipher_ivsize( blk_desc.tfm ) );
	crypto_blkcipher_set_iv( blk_desc.tfm, iv, crypto_blkcipher_ivsize( blk_desc.tfm ) );
	iv[ crypto_blkcipher_ivsize( blk_desc.tfm )+1] = '\0';
	printk(" IV set= %s\n", iv);
	
	// Iv set end

	switch(flag)
	{
		case ENCRYPT_FLAG:
			printk("FILE = %s, LINE = %d, Encryption\n", __FILE__, __LINE__);
			if(crypto_blkcipher_encrypt(&blk_desc,sg2, sg1, len) < 0)
			{
				ret = -EAGAIN;
				printk("Cipher operation could not be completed\n");
				goto OUT_CRYPTO;
			}
			printk("FILE = %s, LINE = %d, Encryption done\n", __FILE__, __LINE__);

			break;

		case DECRYPT_FLAG:
			printk("FILE = %s, LINE = %d, Decryption\n", __FILE__, __LINE__);
			if(crypto_blkcipher_decrypt(&blk_desc, sg2, sg1, len) < 0)
			{
				ret = -EAGAIN;
				printk("Cipher operation could not be completed\n");
				goto OUT_CRYPTO;
			}
			break;

		default:
			printk("FILE = %s, LINE = %d, Default operation not Supported\n", __FILE__, __LINE__);
			ret = -EINVAL;
			goto OUT_CRYPTO;
			break;

	}

OUT_CRYPTO:
	printk("Cipher operation completed wit ret = %d\n", ret);
	if(iv)
		kfree(iv);

	if (blkcipher){
		crypto_free_blkcipher(blkcipher);
		blkcipher = NULL;
	}

	return ret;
}

/**
 * validate_inputs - This funtion is used for validating the inputs from teh user land. 
 * @arg: The strcutured arguments from user land
 * Returns zero on success else error number.
 */
static int validate_inputs(void *arg){
	int ret = 0;
	printk("Received arg %p\n", arg);
	if (arg == NULL)
	{
		ret = -EINVAL;
		goto OUT_VALID;
	}

	if(((userinfo *)arg)->keybuff==NULL || ((userinfo *)arg)->infile==NULL || ((userinfo *)arg)->outfile==NULL || ((userinfo *)arg)->flag < 0 || ((userinfo *)arg)->flag > 1)
	{
		printk("Invalid Arguments from User Space\n");
		ret = -EINVAL;
		goto OUT_VALID;
	}
	else   // we will verify the area and the user space and then length
	{
		ret= access_ok(VERIFY_READ, ((userinfo *)arg)->keybuff, strlen_user(((userinfo *)arg)->keybuff));
		if(!ret )
		{
			printk("Invalid address from User Space keybuff\n");
			ret = -EFAULT;
			goto OUT_VALID;
		}
		ret= access_ok(VERIFY_READ, ((userinfo *)arg)->infile, strlen_user(((userinfo *)arg)->infile));
		if(!ret )
		{
			printk("Invalid address from User Space infile\n");
			ret = -EFAULT;
			goto OUT_VALID;
		}
		ret= access_ok(VERIFY_READ, ((userinfo *)arg)->outfile, strlen_user(((userinfo *)arg)->outfile));
		if(!ret )
		{
			printk("Invalid address from User Space outfile\n");
			ret = -EFAULT;
			goto OUT_VALID;
		}
		ret= access_ok(VERIFY_READ, ((userinfo *)arg)->flag, 0);
		if(!ret )
		{
			printk("Invalid address from User Space flag\n");
			ret = -EFAULT;
			goto OUT_VALID;
		}
		//for extra credits
		ret= access_ok(VERIFY_READ, ((userinfo *)arg)->keybuff, strlen_user(((userinfo *)arg)->ciphertype));
		if(!ret )
		{
			printk("Invalid address from User Space ciphertype\n");
			ret = -EFAULT;
			goto OUT_VALID;
		}
	}

OUT_VALID:
	return ret;
}

asmlinkage extern long (*sysptr)(void *arg);

asmlinkage long xcrypt(void *arg)
{
	int errno = 0, ret = 0;
	unsigned int size = 0 ;
	struct file *filp_in = NULL;
	struct file *filp_tmp = NULL;
	char *inbuf = NULL;
	char *outbuf = NULL;
	char *preamble = NULL;
	char *testbuff = NULL;
	userinfo *uInfo = NULL;
	printk("xcrypt received arg %p\n", arg);
	ret = validate_inputs(arg);
	if(!ret){
		printk("input validation failed, exit\n");
		errno = -EINVAL;
		goto OUT_SUCCESS;

	}
	//now we will copy the data from user space to the structure here.
	uInfo = (userinfo *)kmalloc(sizeof(userinfo), GFP_KERNEL);
	if(uInfo == NULL)
	{
		errno = -ENOMEM;
		goto OUT_SUCCESS;
	}
	printk("Memory allocated = %p\n",uInfo);
	ret = copy_from_user((void*) uInfo, arg, sizeof(userinfo));
	if(ret != 0)
	{
		printk("Could not copy complete from user space\n");
		errno= -EFAULT;
		goto OUT_INFO;
	}

	// opening the input file and out put file
	filp_in = filp_open(uInfo->infile, O_RDONLY, 0);
	if (!filp_in || IS_ERR(filp_in))
	{
		printk("open FILE ERROR %d\n", (int) PTR_ERR(filp_in));
		errno = -EACCES;  /* or do something else */
		goto OUT_FILE;
	}

	if (!filp_in->f_op->read)
	{
		errno = -EPERM;
		goto OUT_FILE;
	}

	// we will copy verything now in the temprory file and later rename that file.
	filp_tmp = filp_open( uInfo->outfile, O_WRONLY|O_CREAT, 0);
	if (!filp_tmp || IS_ERR(filp_tmp))
	{
		printk("WRITE FILE ERROR %d\n", (int) PTR_ERR(filp_tmp));
		errno = -EACCES;  /* or do something else */
		goto OUT_FILE;
	}

	printk("FILE Opened for writing!\n");
	if (!filp_tmp->f_op->write)
	{
		errno = -EPERM;
		goto OUT_FILE;
	}
	//check extra file checks
	printk("FILE is regulr file!\n");
	if( !S_ISREG( filp_in->f_path.dentry->d_inode->i_mode ) )
	{
		printk("FILE IS NOT REGULAR FILE\n" );
		errno = -EBADF;
		goto OUT_FILE;
	}

	printk("File checks if they are same!\n");
	if( ( filp_in->f_path.dentry->d_inode->i_sb == filp_tmp->f_path.dentry->d_inode->i_sb ) &&
			( filp_in->f_path.dentry->d_inode->i_ino ==  filp_tmp->f_path.dentry->d_inode->i_ino ) )
	{
		printk( "FILES CAN'T BE SAME\n" );
		errno = -EINVAL;
		goto OUT_FILE;
	}

	//set the same permission mode
	printk("settiing same pemission mode!\n");
	filp_tmp->f_path.dentry->d_inode->i_mode =  filp_in->f_path.dentry->d_inode->i_mode;

	//key to make it 16 byte
	uInfo->keybuff[16] = 0;
	inbuf = (char *)kmalloc(PAGE_SIZE, GFP_KERNEL);
	if(inbuf == NULL)
	{
		errno = -ENOMEM;
		goto OUT_FILE;
	}

	outbuf = (char *)kmalloc(PAGE_SIZE, GFP_KERNEL);
	if(outbuf == NULL)
	{
		errno = -ENOMEM;
		goto OUT_ALLOC;
	}
	preamble = (char * )kmalloc(16, GFP_KERNEL);
	if(preamble == NULL)
	{
		printk("NO MEMORY!!\n");
		errno = -ENOMEM;
		goto OUT_ALLOC;
	}

	switch(uInfo->flag)
	{

		//encrypt
		case ENCRYPT_FLAG:

			printk(" The Mode:- %d key buf = [%s]\n", uInfo->flag, uInfo->keybuff);
			//memset( preamble, 0, 16 );
			ret =  crypto_hash(uInfo->keybuff, preamble, strlen(uInfo->keybuff));
			printk("hash = %s len = %d\n", preamble, strlen(preamble));
			if(ret != 0)
			{
				printk("hash FAILED!!\n");
				errno = ret;
				goto OUT_ALLOC;
			}

			//write the preamble
			ret = file_op(filp_tmp, preamble,strlen(preamble), WRITE_FLAG);
			printk("Preamble = %s length = %d\n", preamble, ret);
			if(ret <= 0)
			{
				printk("WRITE ERROR PREAMBLE TO USER SPACE\n");
				errno = ret;
				goto OUT_ALLOC;
			} 

			size = (unsigned int)filp_in->f_path.dentry->d_inode->i_size;
			memset( inbuf, 0, PAGE_SIZE);
			memset( outbuf, 0, PAGE_SIZE);
			while((ret = file_op(filp_in, inbuf, PAGE_SIZE, READ_FLAG)) > 0)
			{
				printk("the read string = %s and read bytes = %d\n", inbuf, ret);
				//crypto operation
				ret = crypto_op(inbuf,outbuf, uInfo->keybuff, ret, ENCRYPT_FLAG);
				if(ret < 0)
				{
					printk("ENCRYPTION FAILED!!\n");
					errno = ret;
					goto OUT_ALLOC;
				}

				//write the data
				ret = file_op(filp_tmp, outbuf,strlen(outbuf), WRITE_FLAG);
				printk("the written on temp  string = %s and size = %d\n", outbuf, ret);
				if(ret <= 0)
				{
					printk("WRITE ERROR TO USER SPACE\n");
					errno = ret;
					goto OUT_ALLOC;
				}
				size = size - ret;
				if(size <= 0 ){
					printk("Writing finish in output file \n");
					break;;
				}
				memset( inbuf, 0, PAGE_SIZE);
				memset( outbuf, 0, PAGE_SIZE);
			}

			printk("created the temp file \n");
			break;

			//decrypt
		case DECRYPT_FLAG:
			printk(" The Mode:- %d\n", uInfo->flag);

			//read the preamble size 
			ret = file_op(filp_in, preamble,17, READ_FLAG);
			printk("READ Preamble = %s\n", preamble);
			if(ret <= 0)
			{
				printk("READ ERROR PREAMBLE FROM USER SPACE\n");
				errno = ret;
				goto OUT_ALLOC;
			}

			testbuff = (char *)kmalloc(17, GFP_KERNEL);
			if( testbuff == NULL)
			{
				printk("NO MEMORY!!\n");
				errno = -ENOMEM;
				goto OUT_ALLOC;
			}
			ret =  crypto_hash(uInfo->keybuff, testbuff, strlen(uInfo->keybuff));
			printk("test hash = %s \n", testbuff);
			if(ret != 0)
			{
				printk("hash FAILED!!\n");
				errno = ret;
				goto OUT_ALLOC;
			}

			//verify the preamble
			if(strcmp(testbuff,preamble) != 0)
			{
				printk("VERIFICATION FAILED!!\n");
				errno = ret;
				goto OUT_ALLOC;

			}

			printk(" Verification succedded!\n");
			size = (unsigned int)filp_in->f_path.dentry->d_inode->i_size - strlen(testbuff);
			printk(" Size of input file = %d\n", size);
			memset( inbuf, 0, PAGE_SIZE);
			memset( outbuf, 0, PAGE_SIZE);
			while((ret = file_op(filp_in, inbuf, PAGE_SIZE, READ_FLAG)) > 0)
			{

				printk("the read string = %s and read bytes = %d\n", inbuf, ret);
				//crypto operation
				ret = crypto_op(inbuf,outbuf, uInfo->keybuff, ret, DECRYPT_FLAG);
				if(ret < 0)
				{
					printk("DECRYPTION FAILED!!\n");
					errno = ret;
					goto OUT_ALLOC;
				}

				//write the data
				ret = file_op(filp_tmp, outbuf,strlen(outbuf), WRITE_FLAG);
				printk("the written string = %s and size = %d\n", outbuf, ret);
				if(ret <= 0)
				{
					printk("WRITE ERROR TO USER SPACE\n");
					errno = ret;
					goto OUT_ALLOC;
				}
				size = size - ret;
				if(size <= 0 ){
					printk("Writing finish in out file \n");
					break;
				}
				memset( outbuf, 0, PAGE_SIZE);
				memset( inbuf, 0, PAGE_SIZE);
			}
			break;

			//default
		default:
			printk("wrong flag\n");
			errno = -EINVAL;
			break;
	}


OUT_ALLOC:
	if(testbuff)
	{
		kfree(testbuff);
		testbuff= NULL;
	}
	if(preamble)
	{
		kfree(preamble);
		preamble= NULL;
	}
	if(outbuf)
	{
		kfree(outbuf);
		outbuf= NULL;
	}
	if(inbuf )
	{
		kfree(inbuf);
		inbuf= NULL;
	}
	
OUT_FILE:

	if(filp_tmp){
		filp_close(filp_tmp, NULL);
	}

	if(filp_in){
		filp_close(filp_in, NULL);
	}

OUT_INFO:
	if(uInfo)
	{
		kfree(uInfo);
		uInfo = NULL;
	}

OUT_SUCCESS:
	printk("sys_xcrypt module returning = %d\n", errno);
	return errno;
}

static int __init init_sys_xcrypt(void)
{
	printk("installed new sys_xcrypt module\n");
	if (sysptr == NULL)
		sysptr = xcrypt;
	return 0;
}

static void  __exit exit_sys_xcrypt(void)
{
	if (sysptr != NULL)
		sysptr = NULL;
	printk("removed sys_xcrypt module\n");
}
module_init(init_sys_xcrypt);
module_exit(exit_sys_xcrypt);
MODULE_LICENSE("GPL");
